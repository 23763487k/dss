const express = require( 'express');
const app = express()
var bodyParser = require('body-parser')
app.use(bodyParser.json())

const es6Renderer = require('express-es6-template-engine');
app.engine('html', es6Renderer);
app.set('views', 'views');
app.set('view engine', 'html')


app.get('/hello', ( request, response )=> {
    response.send( 'Hello World!!!')
});

app.get('/goodbye', ( request, response )=> {
    response.send( 'bye World!!!')
});

app.get('/v1/api/obtenerResultados', ( request, response )=> {
    const arr = []
    for(i = 1;i<10;++i){
        arr.push(i)
    }
    response.send(arr);
});
app.get('/sayMyName',(request,response)=>{
    const myName = request.query
    console.log(myName)
    response.send(`Mi nombre es ${myName.name} ${myName.apellido}`)
})

app.get('/direccion',(request,response)=>{
    const direccion = request.query
    console.log(direccion)
    response.send(`Mi direccion es: 
    Calle: ${direccion.calle} 
    Numero: ${direccion.numero},
    Ciudad: ${direccion.ciudad},
    Region: ${direccion.region},`)
})

app.post('/pruebaPost',(request,response)=>{
    console.log(request.body)
    response.send(`Nombre ${request.body.nombre}`)
})

app.get("/sumar",(request,response)=>{
    const {numero1,numero2,numero3,numero4} = request.query
    console.log(numero4)
    const suma = parseInt(numero1) 
    + parseInt(numero2) 
    + parseInt(numero3)
    response.send(`La suma es ${suma}`)
})
app.post("/sumar",(request,response)=>{
    const {numero1,numero2,numero3} = request.body
    console.log(request.body)
    // console.log(numero1)
    // console.log(numero2)
    // console.log(numero3)
    const suma = parseInt(numero1) 
    + parseInt(numero2) 
    + parseInt(numero3)
    response.send(`La suma es ${suma}`)
})
app.post("/sumarArr",(request,response)=>{
    const {arrNum} = request.body
    var suma = 0
    arrNum.forEach(numero => {
        suma+=numero
    });
    response.send(`La suma es ${suma}`)
})

app.get('/holamundo',(request,response)=>{
    console.log ('holaMundo')
})

app.get('/login',(request,response) =>{
    response.render('login')
})

app.listen (3000, ()=>{
console. log('Listening at localhost: 3000')
})